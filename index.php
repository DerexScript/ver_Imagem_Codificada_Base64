<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Exibir imagem codificada Base64 - PHP</title>
</head>
<body>

	<?php
    	// LÊ O ARQUIVO E RETORNA UMA STRING
    	$imagem = file_get_contents('escape-flight.png');

    	// CONVERTE A STRING GERADA PARA BASE64
    	$imagem64 = base64_encode($imagem);
	?>

	<!-- <img src='data:image/png;base64, <?php// echo $imagem64 ?>'> -->
	<img src='data:image/png;base64, <?php echo $imagem64 ?>'>
</body>
</html>